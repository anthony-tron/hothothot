TRON Anthony <anthony.tron@etu.univ-amu.fr>
Licence professionnelle DEVWEB
Année universitaire 2021-2022

Institut Universitaire Technologique d'Aix-en-Provence
413 Avenue Gaston Berger
https://iut.univ-amu.fr

ECUE11 - Langages pour le Web
M. GÉRARD Olivier <oliv.gerard@gmail.com>

# # # # #

Le site est hébergé sur GitLab Pages : https://anthony-tron.gitlab.io/hothothot/

# Tester le projet :

## Prérequis

- node v14.18.1 (lts/fermium)
- npm

## Commandes

npm i
npm run dev # lancement d'un serveur de développement
npm run build # création de l'application prête à déployer dans build/
npm run purgecss # purification du css


# HTML

Au début, j'avais des connaissances assez primaires. Je savais structurer
et indenter mon document correctement, mais je n'utilisais que les
features les plus basiques. De plus, je faisais souvent des
"soupes de <div>".

Dans ce projet, j'ai pu découvrir que les web components étaient
véritablement supportés, je les ai donc utilisés tout en les couplant
avec l'attribut role. J'avais connaissance de cet attribut mais je ne
comprenais pas vraiment son utilité avant. Également, j'ai découvert
quelques attributs ARIA, et m'a fait comprendre à quel point il faut penser 
à tous les utilisateurs pour rendre le contenu le plus accessible à tous.


# CSS

J'ai déjà écrit plusieurs fois des petites librairies CSS et SCSS pour des
petits projets mais comme cela ne s'était toujours pas bien passé, j'avais
décidé d'abandonner ce genre d'idée et de plutôt utiliser des framework à
la place. J'avais déjà pu expérimenté bootstrap, materialcss, et
tailwindcss avant le début des cours.

En revanche, je n'avais pas poussé les limites de SASS, ce que j'ai tenté
dans ce projet en écrivant mon propre framework SCSS extrêmement
inspiré de tailwindcss (que j'ai d'ailleurs nommé not-tailwind).
J'ai donc pu expérimenté les @mixin, les boucles avec @each, et la
clause @use 'module' with <mes_variables>.
Cela génère une énorme feuille de style de plus de 100 kilobytes dans
la plupart des cas, mais cela est destiné à l'utilisation d'un outil
supplémentaire de "purification" de css dont le choix n'est pas imposé
par les utilisateurs de la librairie (si tenté que quelqu'un d'autre
utilise ma librairie un jour).

J'ai pu mettre en place une purification du CSS qui consiste à filtrer
les classes CSS non utilisées.

Conclusion :
Après avoir développé ce framework, je suis finalement rétissant à l'utilisation
de ce genre de framework. Certes, il permet de styliser rapidement, mais rend
le squelette HTML pollué surtout en ce qui concerne les design complexes. De
plus, pour factoriser les répétitions de classe, on pourrait créer une classe
SCSS en incluant (@include) les classes utilisées, mais... cela ne revient-il
pas à la base du problème ? Que devient le gain par rapport à l'utilisation
de CSS ou SCSS dite classique ? À méditer.


# JavaScript

JavaScript est probablement le langage avec lequel j'ai du le plus
travaillé pour des projets personnels. J'ai déjà fait des projets utilisant
webpack, d'autres utilisant simplement NodeJS, du TypeScript. J'ai
déjà développé en natif (gestion des évènements, gestion du localStorage "à
la main"), en React, et un peu en jQuery. Je suis assez à l'aise avec
les "nouvelles" fonctionnalités du langage, je n'hésite donc pas à
destructurer mes tableaux et mes objets, imbriquer des fonctions,
utiliser les promesses, etc.

Je suis directement parti dans l'idée d'utiliser le plus de nouvelles
features récentes. Par exemple, j'utilise les modules JavaScript qui
permettent d'utiliser la syntaxe import et export. J'utilise également
l'API Fetch plutôt que XMLHttpRequest car je ne l'avais jamais utilisé
auparavant.
La véritable nouveauté a été d'utiliser les template. J'avais déjà
auparavant regardé leur documentation, mais pas très convaincu, je les
avais ignorés. Je trouve aujourd'hui leur utilisation peu évidente et peu
répendue. Il faudrait à mon avis attendre l'arrivée d'une proposition
d'une API uniformisée pour les utiliser convenablement.

J'ai tenté de développer une API orientée composant (influencé principalement
par React). Pour faire court, un composant est une fonction qui n'a qu'une
seule contrainte : renvoyer une fonction sans arguments de démontage.
Cette fonction est utilisé pour se désabonner des events, des interval,
et/ou pour restituer le DOM dans l'état qu'il était avant le montage du
composant.

Ex.
export default function ticTac() {
    // montage
    const id = setTimeInterval(() => console.log('tick'), 1000);

    return () => {
        // démontage
        clearInterval(id);
    };
}

Cette API dispose de nombreuses limitations : elle se contente de
reconstruire le composant à chaque fois en éxécutant entièrement la fonction.
Il faut de plus manuellement appeler la fonction de démontage. On pourrait
imaginer une amélioration de cette API fonctionnelle pour faire en sorte
que le minimum nécessaire subisse des mises à jour.
Le développement de cette API n'a pour objectif que la recherche et ne
devrait pas être utilisé pour un vrai projet.


# Autres

Pour ce projet, j'ai mis en place une pipeline grâce GitLab CI dont le fichier
de configuration est à la racine du projet.
Il permet de tester la validation W3C du fichier src/index.html à chaque push,
et de déployer le site (à condition de push sur la branch `ci/pages`).


# Bilan personnel

Ce projet a été très riche en découverte sur le plan SCSS. J'ai pu avoir un
aperçu du développement d'un framework de style.

Côté JavaScript, la seule découverte reste celle des template. J'ai également
pu expérimenté le routage côté front fait avec JavaScript, ce qui est assez
puissant. En somme, j'ai une idée de ce que font les framework de routing
côté client.

Ma plus grande progression reste le fait d'utiliser les Web Components et
l'utilisation des attributs liés à ARIA. J'ai une véritable compréhension
de comment améliorer l'accessibilité d'un site.

Enfin, niveau gestion de projet, j'ai pu mettre en application les normes
de nommage que j'utilise en entreprise, ce qui facilite la lecture des commit
et des branches. Ce dépôt git est constitué de plus de 100 commits dont chacun
a un but précis, afin de pouvoir détecter facilement quel commit a apporté
un bug, et de pouvoir revert sans perdre des fonctionnalités.

Pour conclure, tout faire soi-même ce n'est pas si facile qu'on pourrait le
croire mais on s'en doutait !
