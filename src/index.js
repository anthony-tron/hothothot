'use strict';

import header from './components/header';
import layout from './components/layout';
import dashboard from './pages/dashboard';
import stats from './pages/stats';

const route = (() => {
    let unmount;

    return () => {
        unmount?.();

        // match last token
        // limitation: only match routes with lowcase letters
        switch (document.location.pathname.match(/\/[a-z]*\/?$/)?.[0]) {
            case '/stats/':
            case '/stats':
                unmount = layout({ children: [
                    header(document.body, 1),
                    stats(document.querySelector('body > main')),
                ]});
                break;
        
            default:
                unmount = layout({ children: [
                    header(document.body, 0),
                    dashboard(document.querySelector('body > main')),
                ]});
                break;
        }
    };
})();

route();

window.addEventListener('popstate', route);
