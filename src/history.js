export const push = (url) => {
    window.history.pushState({}, '', url);
    window.dispatchEvent(new Event('popstate'));
};
