'use strict';

import createSensor from '../components/sensor';

import createAppStatus from '../components/appStatus';

import { DATA_ENDPOINT } from '../constants';
import { parseSensors } from '../dto/sensors';

export default function dashboard(parent) {
    let intervalId;
    let appStatus;
    let sensors = {};

    // init sensors
    fetch(DATA_ENDPOINT)
        .then(response => response.json())
        .then(parseSensors)
        .then((data) => {
            // instantiate sensors

            appStatus = createAppStatus(parent, data.message);

            Object.entries(data.sensors).forEach(([id, sensorData]) => {
                sensors[id] = {
                    values: [sensorData.value],
                    element: createSensor(parent, sensorData.title, sensorData.unit, [sensorData.value]),
                };
            });

        })
        .then(() => {
            intervalId = setInterval(() => {
            
                fetch(DATA_ENDPOINT)
                    .then(response => response.json())
                    .then(parseSensors)
                    .then(data => {
                        appStatus();
                        appStatus = createAppStatus(parent, data.message);

                        Object.entries(data.sensors).forEach(([id, {title, value, unit}]) => {
                            sensors[id].values.push(value);
                            if (sensors[id].values.length === 11) {
                                sensors[id].values.shift();
                            }
                            sensors[id].element(); // unmount
                            sensors[id].element = createSensor(parent, title, unit, sensors[id].values);
                        });
                    })
            }, 1000); // TODO, replace by user preference
        })
        .catch(e => {
            // do something
            alert('ERROR: ' + e.message);
        });
    
    return () => {
        appStatus();
        Object.values(sensors).forEach(({ element }) => element());
        clearInterval(intervalId);
    };

}
