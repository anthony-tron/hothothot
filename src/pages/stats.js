import Chart from 'chart.js/auto';
import { parseHours, parseDatasets } from '../dto/stats';
import { DATA_ENDPOINT, colors } from '../constants';

export default function stats(parent) {
    const ctx = document.createElement('canvas');
    parent.appendChild(ctx);

    fetch(`${DATA_ENDPOINT}/stats.json`)
        .then(data => data.json())
        .then(data => [
            parseHours(data),
            parseDatasets(data),
        ])
        .then(([labels, datasets]) => {

            const chart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels,
                    datasets: datasets.map((dataset, i) => ({
                        ...dataset,
                        borderColor: colors[i % colors.length],
                    })),
                },
            });
        });
    
    return () => {
        ctx.remove();
    };
}
