export default function sensor(parent, title, unit, values) {
    parent.appendChild(createSensor(title, unit, values));
    const instance = parent.lastElementChild;

    return () => {
        instance.remove();
    };
}

export const createSensor = (title, unit, values) => {
    const fragment = document
        .getElementById('sensor')
        .content
        .cloneNode(true);

    fragment
        .querySelector('sensor-title')
        .textContent = title;

    fragment
        .querySelector('sensor-value')
        .firstChild
        .textContent = values[values.length - 1] + unit; // result should be a String
    
    values.forEach((value) => {
        updateValue(fragment, value);
    });

    return fragment;
};

export const updateValue = (fragment, value) => {
    
    const list = fragment
        .querySelector('sensor-history');

    const newElement = list.firstElementChild.cloneNode(true);
    newElement.style.height = `${value}px`;
    const dataElement = newElement.querySelector('data');
    dataElement.setAttribute('value', value);

    list.firstChild.before(newElement);
}
