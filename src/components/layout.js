export default function layout({ children }) {

    return () => {
        children.forEach(unmount => unmount());
    }
}
