import { push } from '../history';

export default function header(parent, selectedTabIndex) {
    const fragment = document
        .getElementById('header')
        .content
        .cloneNode(true);

    parent.firstChild.before(fragment);
    const instance = parent.firstElementChild;

    instance.querySelectorAll('a').forEach((link) => {
        link.addEventListener('click', (event) => {
            event.preventDefault();
            push(link.getAttribute('href'));
        })
    });

    instance.querySelectorAll('tab').forEach((tab, i) => {

        if (i === selectedTabIndex) {
            tab.classList.add('bg-white');
            tab.querySelector('a').classList.add('text-blue-60');
        } else {
            tab.querySelector('a').classList.add('text-white');
        }
    })

    return () => {
        instance.remove();
    };
};
