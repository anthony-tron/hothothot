export default function appStatus(parent, { type, short, full }) {

    parent.appendChild(createAppStatus({ type, short, full }));
    const instance = parent.lastElementChild;

    return () => {
        instance.remove();
    };
}

export const createAppStatus = ({type, short, full}) => {
    const fragment = document
        .getElementById('app-status')
        .content
        .cloneNode(true);

    updateAppStatus(fragment, {
        type,
        short,
        full,
    });

    return fragment;
};

export const updateAppStatus = (appStatus, {type, short, full}) => {
    const indicator = appStatus.querySelector('indicator');

    switch (type) {
        case 'ok':
            indicator.textContent = '✓ OK';
            indicator.setAttribute('aria-label', 'ok');
            indicator.classList.add('text-green-50')
            indicator.classList.remove('text-red-50')
            break;
        case 'alert':
            indicator.textContent = '⚠ Alert';
            indicator.setAttribute('aria-label', 'alert');
            indicator.classList.add('text-red-50')
            indicator.classList.remove('text-green-50')
            break;
    }

    // update message
    const shortMessage = appStatus.querySelector('details > summary');
    shortMessage.textContent = short;

    // full message?
    const fullMessage = appStatus.querySelector('details > p');
    fullMessage.textContent = full;
};
