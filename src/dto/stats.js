/**
 * parseHours
 * @param {object} data 
 * @returns Array sorted
 */
export const parseHours = (data) => {
    return [...new Set(
        Object
            .values(data.sensors)
            .map(sensor =>
                sensor.data
                    .map(entry => entry.hour)
            )
            .reduce((acc, label) => [...acc, ...label])
    )].sort();
};

/**
 * parseDatasets
 * @param {object} data to parse
 * @returns {Array} of {data, label}
 */
export const parseDatasets = (data) => {
    return Object
        .entries(data.sensors)
        .map(([sensorKey, sensorData]) => ({
            data: sensorData.data
                .map(entry => entry.value),
            label: sensorKey,
        }));
};
