export const parseSensors = (data) => ({
    sensors: data.sensors,
    message: data.message || {
        type: 'ok',
        short: 'Nothing to report.',
        full: 'Everything is fine',
    }
});
