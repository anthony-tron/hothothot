// Snowpack Configuration File
// See all supported options: https://www.snowpack.dev/reference/configuration

/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
  mount: {
    src: {
        url: '/',
    },
    static: {
        url: '/',
        static: true,
    },
  },
  plugins: [
    [
      '@snowpack/plugin-sass',
      {
        native: true,
        compilerOptions: {
          // compress for production only
          style: process.env.NODE_ENV === 'production' ? 'compressed' : 'expanded',
        },
      },
    ],
  ],
  packageOptions: {
    /* ... */
  },
  devOptions: {
    /* ... */
  },
  buildOptions: {
    baseUrl: process.env.PUBLIC_URL || '/', // sets PUBLIC_URL for the snowpack project
  },
};
